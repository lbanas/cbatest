<?php namespace Controllers;

use Classes\Controller;
use Exceptions\OrderException;
use Models\Customer;
use Models\DeliveryMethod;

class OrderController extends Controller
{
    /**
     * @throws OrderException
     * @throws \Exceptions\ViewNotFoundException
     */
    public function deliveryAction() : void
    {
        $deliveryMethods = DeliveryMethod::findAll();
        $customer = $this->getCustomer();
        $cart = $this->createCartFromSession($customer);
        $request = $this->getRequest();

        if ($cart !== null && $request->isPost())
        {
            $method = (int) $request->getPost('selected_delivery');
            $deliveryMethod = DeliveryMethod::find($method);

            if (empty($deliveryMethod))
            {
                throw new OrderException('Please select delivery method');
            }

            $cart->setDeliveryMethod($deliveryMethod);
            $this->redirect('/order/confirm');
        }

        $this->render('OrderDelivery', [
            'delivery_methods' => $deliveryMethods,
            'customer' => $customer,
            'cart' => $cart
        ]);
    }

    /**
     * @throws OrderException
     * @throws \Exceptions\ViewNotFoundException
     */
    public function confirmAction() : void
    {
        $customer = $this->getCustomer();
        $cart = $this->createCartFromSession($customer);

        $this->render('OrderConfirm', [
            'customer' => $customer,
            'cart' => $cart
        ]);
    }

    /**
     * @throws OrderException
     */
    public function orderAction() : void
    {
        $request = $this->getRequest();
        $customer = $this->getCustomer();
        $cart = $this->createCartFromSession($customer);

        if ($request->isPost())
        {
            $founds = $customer->getFounds();

            if ($cart->getTotal() > $founds)
            {
                $this->redirectWithMessage('/home/display', 'You don`t have enough founds');
            }

            $cart->convertToOrder();
            $this->redirectWithMessage('/home/display', 'Successfully placed order');
        }

        $this->redirect('/home/display');
    }

    /**
     * @return Customer
     * @throws OrderException
     */
    public function getCustomer() : Customer
    {
        $customer = Customer::createFromSession();

        if (!$customer)
        {
            throw new OrderException('Only registered users can buy');
        }

        return $customer;
    }
}