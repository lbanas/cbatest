<?php namespace Controllers;

use Classes\Controller;
use Exceptions\ProductNotFoundException;
use Models\Customer;
use Models\Product;

class ProductController extends Controller
{
    /**
     * @throws ProductNotFoundException
     * @throws \Exceptions\ViewNotFoundException
     */
    public function displayAction() : void
    {
        $product = $this->getProductByGetId();
        $customer = Customer::createFromSession();
        $cart = $customer ? $this->createCartFromSession($customer) : null;
        $canRate = $product->canCustomerRate($customer);

        $this->render('Product', [
            'product' => $product,
            'customer' => $customer,
            'cart' => $cart,
            'can_rate' => $canRate,
            'customer_rating' => (!$canRate && $customer) ? $product->getCustomerRating($customer) : 0
        ]);
    }

    /**
     * @throws ProductNotFoundException
     * @throws \Exceptions\ApplicationException
     */
    public function rateAction() : void
    {
        $product = $this->getProductByGetId();
        $rating = (int) $this->getRequest()->getPost('rate', 1);
        $product->setRating($rating);
        $customer = Customer::createFromSession();
        $product->setRatedBy($customer, $rating);

        $this->redirectWithMessage('/product/display?id=' . $product->getId(), 'Thanks for rating!');
    }

    /**
     * @return \Interfaces\DatabaseModelInterface|Product|null
     * @throws ProductNotFoundException
     */
    private function getProductByGetId()
    {
        $request = $this->getRequest();
        $productId = (int) $request->getGet('id', 0);
        $product = Product::find($productId);

        if ($product === null)
        {
            throw new ProductNotFoundException('Product not found');
        }

        return $product;
    }
}