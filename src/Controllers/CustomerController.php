<?php namespace Controllers;

use Classes\Controller;
use Models\Customer;

class CustomerController extends Controller
{

    /**
     * @throws \Exceptions\AuthorizationException
     */
    public function loginAction() : void
    {
        $request = $this->getRequest();

        if ($request->isPost() && !empty($request->getPost('login')) && !empty($request->getPost('password'))) {
            $customer = new Customer();
            $customer->logIn($request->getPost('login'), $request->getPost('password'));
            $this->redirect('/home/display');
        }
    }

    public function logoutAction() : void
    {
        $customer = Customer::createFromSession();

        if ($customer) {
            $customer->logOut();
        }

        $this->redirect('/home/display');
    }
}