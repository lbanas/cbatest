<?php namespace Controllers;

use Classes\Controller;
use Exceptions\CartException;
use Exceptions\ProductNotFoundException;
use Models\Customer;
use Models\Product;

class CartController extends Controller
{
    /**
     * @throws CartException
     * @throws ProductNotFoundException
     */
    public function addAction() : void
    {
        $request = $this->getRequest();
        $customer = $this->getCustomer();
        $cart = $this->createCartFromSession($customer);

        if ($request->isPost())
        {
            $product = $this->getProductById((int) $request->getPost('product_id', 0));
            $cart->addProduct($product, (int) $this->getRequest()->getPost('product_quantity', 1));

            $this->redirectWithMessage(
                '/product/display?id=' . $product->getId(),
                'Successfully added product to cart'
            );
        }

        throw new CartException('No post data on add method');
    }

    /**
     * @throws CartException
     * @throws ProductNotFoundException
     */
    public function removeAction() : void
    {
        $request = $this->getRequest();
        $customer = $this->getCustomer();
        $cart = $this->createCartFromSession($customer);
        $product = $this->getProductById((int) $request->getGet('id'));
        $cart->removeProduct($product);

        $this->redirectWithMessage(
            '/product/display?id=' . $product->getId(),
            'Successfully removed product form cart'
        );
    }

    /**
     * @throws CartException
     * @throws ProductNotFoundException
     */
    public function subtractAction() : void
    {
        $request = $this->getRequest();
        $customer = $this->getCustomer();
        $cart = $this->createCartFromSession($customer);

        if ($request->isPost())
        {
            $product = $this->getProductById((int) $request->getPost('product_id', 0));
            $quantity = (int) $this->getRequest()->getPost('product_quantity', 1);
            $cart->removeProduct($product, $quantity);

            $this->redirectWithMessage(
                '/product/display?id=' . $product->getId(),
                'Successfully removed product form cart'
            );
        }

        throw new CartException('Product removal is available through post method only');
    }

    /**
     * @throws CartException
     */
    public function resetAction() : void
    {
        $customer = $this->getCustomer();
        $cart = $this->createCartFromSession($customer);
        $cart->clearCart();

        $this->redirectWithMessage(
            '/home/display',
            'Successfully cleared cart'
        );
    }

    /**
     * @return \Interfaces\DatabaseModelInterface|Product|null
     * @throws ProductNotFoundException
     */
    private function getProductById($productId)
    {
        $product = Product::find($productId);

        if ($product === null)
        {
            throw new ProductNotFoundException('Product not found');
        }

        return $product;
    }

    /**
     * @return Customer
     * @throws CartException
     */
    public function getCustomer() : Customer
    {
        $customer = Customer::createFromSession();

        if (!$customer)
        {
            throw new CartException('Only registered users can buy');
        }

        return $customer;
    }
}