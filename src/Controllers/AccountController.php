<?php namespace Controllers;

use Classes\Controller;
use Exceptions\AuthorizationException;
use Models\Customer;

class AccountController extends Controller
{
    /**
     * @throws AuthorizationException
     * @throws \Exceptions\ViewNotFoundException
     */
    public function displayAction() : void
    {
        $customer = Customer::createFromSession();

        if ($customer === null)
        {
            throw new AuthorizationException('You must be logged to access your account');
        }

        $this->render('Account', [
            'orders' => $customer->orders()->get(),
            'customer' => $customer
        ]);
    }
}