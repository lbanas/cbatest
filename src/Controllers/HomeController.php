<?php namespace Controllers;

use Classes\Controller;
use Exceptions\ViewException;
use Models\Customer;
use Models\Product;

class HomeController extends Controller
{
    /**
     * @throws ViewException
     */
    public function displayAction() : void
    {
        $allProducts = Product::findAll();
        $customer = Customer::createFromSession();
        $cart = $customer ? $this->createCartFromSession($customer) : null;

        $this->render('Homepage', [
            'products' => $allProducts,
            'customer' => $customer,
            'cart' => $cart
        ]);
    }
}