<?php namespace Interfaces;

use Models\Customer;

interface CustomerInterface
{
    public function subtractFounds($value) : void;

    public function resetFounds() : void;

    public function logIn($login, $password) : Customer;

    public function logOut() : void;

    public function isLogged() : bool;
}