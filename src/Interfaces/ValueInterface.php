<?php namespace Interfaces;

interface ValueInterface
{
    public function getPrice() : float;
}