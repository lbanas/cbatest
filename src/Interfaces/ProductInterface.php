<?php namespace Interfaces;

interface ProductInterface
{
    public function setRating(int $value);
}