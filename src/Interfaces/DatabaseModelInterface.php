<?php namespace Interfaces;


interface DatabaseModelInterface
{
    public function save(array $options = []);

    public static function find($id) : ?DatabaseModelInterface;

    public static function findAll() : ?\Countable;
}