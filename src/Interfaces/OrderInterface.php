<?php namespace Interfaces;

use Models\Customer;

interface OrderInterface
{
    public const STATUS_NEW = 1;

    public const STATUS_PROCESSING = 5;

    public const STATUS_PAYED = 10;

    public const STATUS_ABORTED = 15;

    public function getItems() : \Countable;

    public function getStatus() : int;

    public function getCustomer() : Customer;

    public function getTranslatedStatus() : string;
}