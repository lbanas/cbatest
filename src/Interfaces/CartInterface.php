<?php namespace Interfaces;

use Models\Customer;
use Models\DeliveryMethod;
use Models\Product;

interface CartInterface
{
    public function setCustomer(Customer $customer) : void;

    public function addProduct(Product $product, int $quantity) : void;

    public function removeProduct(Product $product, int $quantity = null) : void;

    public function clearCart() : void;

    public function getProducts() : ?array;

    public function setDeliveryMethod(DeliveryMethod $deliveryMethod) : void;

    public function convertToOrder() : void;

    public function getTotal() : float;
}