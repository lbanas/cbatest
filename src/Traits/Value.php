<?php namespace Traits;

trait Value
{
    /**
     * @return float
     */
    public function getPrice() : float
    {
        return ($this->price / 100);
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price) : void
    {
        $this->setAttribute('price', $price * 100);
    }

    /**
     * @return string
     */
    public function getPriceAsString() : string
    {
        return number_format($this->getPrice(), 2) . $this->getCurrencySymbol();
    }
}