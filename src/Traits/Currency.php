<?php namespace Traits;


trait Currency
{
    public function getCurrencySymbol() : string
    {
        return '$';
    }
}