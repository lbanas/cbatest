<?php namespace Models;

use Classes\Model;
use Interfaces\OrderInterface;
use Interfaces\ValueInterface;
use Traits\Currency;
use Traits\Value;

class Order extends Model implements OrderInterface
{
    use Currency;

    protected $table = 'orders';

    public function items()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function delivery_method()
    {
        return $this->belongsTo(DeliveryMethod::class);
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function getCustomer(): Customer
    {
        return $this->customer()->first();
    }

    public function getItems(): \Countable
    {
        return $this->items()->get();
    }

    public function getTranslatedStatus() : string
    {
        switch ($this->status)
        {
            case self::STATUS_PROCESSING:
                return 'processing';
                break;
            case self::STATUS_PAYED:
                return 'payed';
                break;
            case self::STATUS_ABORTED:
                return 'aborted';
                break;
            case self::STATUS_NEW:
            default:
                return 'new';
                break;
        }
    }

    public function getTotal() : float
    {
        return $this->total / 100;
    }

    public function getTotalAsString() : string
    {
        return number_format($this->getTotal(), 2) . $this->getCurrencySymbol();
    }

    public function getDeliveryMethodAsString() : string
    {
        /** @var DeliveryMethod $delivery */
        $delivery = $this->delivery_method()->first();

        return 'Delivery: ' . $delivery->getName() . ', ' . $delivery->getPriceAsString();
    }
}
