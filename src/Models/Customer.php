<?php namespace Models;

use Classes\Model;
use Exceptions\AuthorizationException;
use \Interfaces\CustomerInterface;
use Traits\Currency;

class Customer extends Model implements CustomerInterface
{
    use Currency;

    protected $table = 'customers';

    public $timestamps = false;

    //relations

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    //methods
    /**
     * @return Customer|null
     */
    public static function createFromSession() : ?self
    {
        if (!empty($_SESSION['customer_id'])) {
            return static::find($_SESSION['customer_id']);
        }

        return null;
    }

    /**
     * @param $login
     * @param $password
     * @return Customer
     * @throws AuthorizationException
     */
    public function logIn($login, $password) : Customer
    {
        $customer = $this->findByLoginAndPassword($login, $this->hashPassword($password));

        if ($customer === null)
        {
            throw new AuthorizationException('User with such credentials not found');
        }

        $this->authorizeCustomer($customer);

        return $customer;
    }

    /**
     * @return bool
     */
    public function isLogged() : bool
    {
        return (!empty($_SESSION['customer_id']) && $_SESSION['customer_id'] == $this->id);
    }

    public function logOut() : void
    {
        unset($_SESSION['user']);
        session_destroy();
        header('Location: /home/display');
    }

    /**
     * @param $value
     */
    public function subtractFounds($value) : void
    {
        $this->founds -= $value * 100;
        $this->save();
    }

    /**
     *
     */
    public function resetFounds() : void
    {
        $this->founds = 10000;
        $this->save();
    }

    /**
     * @return float
     */
    public function getFounds() : float
    {
        return $this->founds / 100;
    }
    /**
     * @return string
     */
    public function getFoundsAsString() : string
    {
        return number_format($this->getFounds(), 2) . $this->getCurrencySymbol();
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->login;
    }

    /**
     * @param Customer $customer
     */
    private function authorizeCustomer(Customer $customer)
    {
        session_regenerate_id();
        $_SESSION['customer_id'] = $customer->id;
    }

    /**
     * @param $login
     * @param $hashedPassword
     * @return Customer|null
     */
    private function findByLoginAndPassword($login, $hashedPassword): ?Customer
    {
        return static::query()
            ->where('login', $login)
            ->where('password', $hashedPassword)
            ->first();
    }

    /**
     * @param $password
     * @return string
     */
    final private function hashPassword(&$password)
    {
        return md5($password);
    }
}
