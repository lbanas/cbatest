<?php namespace Models;

use Classes\Model;
use Exceptions\CartException;
use Illuminate\Database\Capsule\Manager as DB;
use Interfaces\CartInterface;
use Traits\Currency;

class Cart extends Model implements CartInterface
{
    use Currency;

    /**
     * @var Customer|null
     */
    private $customer;

    private $deliveryMethod;

    private $products = [];

    public function __destruct()
    {
        $_SESSION['cart'] = (string) $this;
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        $toStore = [];

        foreach ($this->products as $productId => $product)
        {
            $toStore[$productId] = $product['quantity'];
        }

        if ($this->deliveryMethod !== null)
        {
            $toStore['delivery'] = $this->getDeliveryMethod()->getId();
        }

        return json_encode($toStore);
    }

    /**
     * @param string $encodedData
     */
    public function loadFromSession(string $encodedData)
    {
        $data = json_decode($encodedData, true);

        if (isset($data['delivery']))
        {
            $deliveryMethod = DeliveryMethod::find($data['delivery']);

            if (!empty($deliveryMethod))
            {
                $this->setDeliveryMethod($deliveryMethod);
            }
        }

        foreach ($data as $id => $quantity)
        {
            $product = Product::find($id);

            if ($product)
            {
                $this->addProduct($product, (int) $quantity);
            }
        }
    }

    /**
     * @param Product $product
     * @param int $quantity
     */
    public function addProduct(Product $product, int $quantity): void
    {
        if (array_key_exists($product->getId(), $this->products))
        {
            $this->products[$product->getId()]['quantity'] += $quantity;
        }
        else
        {
            $this->products[$product->getId()] = [
                'product' => $product,
                'quantity' => $quantity
            ];
        }
    }

    /**
     * @return array|null
     */
    public function getProducts(): ?array
    {
        return $this->products;
    }

    /**
     * @param Product $product
     * @param int $quantity
     * @throws CartException
     */
    public function removeProduct(Product $product, int $quantity = null): void
    {
        if (!array_key_exists($product->getId(), $this->products))
        {
            throw new CartException('Product with that id not found in cart!');
        }

        if ($quantity === null || $quantity > $this->products[$product->getId()]['quantity'])
        {
           $quantity = $this->products[$product->getId()]['quantity'];
        }

        if ($this->products[$product->getId()]['quantity'] === $quantity)
        {
            unset($this->products[$product->getId()]);
        }
        else
        {
            $this->products[$product->getId()]['quantity'] -= $quantity;
        }

        //Remove all data if cart is empty
        if (empty($this->products))
        {
            $this->clearCart();
        }
    }

    /**
     * @param Customer $customer
     */
    public function setCustomer(Customer $customer): void
    {
        $this->customer = $customer;
    }

    /**
     * @return float
     */
    public function getTotal() : float
    {
        $total = 0;

        foreach ($this->getProducts() as $entry)
        {
            $total += $entry['product']->getPrice() * $entry['quantity'];
        }

        if ($this->getDeliveryMethod())
        {
            $total += $this->getDeliveryMethod()->getPrice();
        }

        return $total;
    }

    /**
     * @return string
     */
    public function getTotalAsString() : string
    {
        return number_format($this->getTotal(), 2) . $this->getCurrencySymbol();
    }

    /**
     * @return \Countable|null
     */
    public function getAvailableDeliveryMethod()
    {
        return DeliveryMethod::findAll();
    }

    /**
     * @return DeliveryMethod|null
     */
    public function getDeliveryMethod(): ?DeliveryMethod
    {
        return $this->deliveryMethod;
    }

    /**
     * @param DeliveryMethod $deliveryMethod
     */
    public function setDeliveryMethod(DeliveryMethod $deliveryMethod): void
    {
        $this->deliveryMethod = $deliveryMethod;
    }

    public function clearCart(): void
    {
        $this->products = [];
        $this->deliveryMethod = null;
    }

    public function convertToOrder(): void
    {
        DB::transaction(function () {
            $order = new Order();
            $order->customer()->associate($this->customer);
            $order->delivery_method()->associate($this->getDeliveryMethod());
            $order->status = Order::STATUS_NEW;
            $order->total = $this->getTotal() * 100;
            $order->save();

            foreach ($this->getProducts() as $product)
            {
                $orderItem = new OrderItem();
                $orderItem->product()->associate($product['product']);
                $orderItem->quantity = $product['quantity'];
                $orderItem->order()->associate($order);
                $orderItem->setPrice($product['product']->getPrice() * $product['quantity']);
                $orderItem->save();
            }

            $this->customer->subtractFounds($this->getTotal());
        });

        $this->clearCart();
    }
}
