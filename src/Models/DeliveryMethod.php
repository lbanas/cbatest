<?php namespace Models;

use Classes\Model;
use Interfaces\ValueInterface;
use Traits\Currency;
use Traits\Value;

class DeliveryMethod extends Model implements ValueInterface
{
    use Currency;

    use Value;

    protected $table = 'delivery_methods';

    public $timestamps = false;

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }
}
