<?php namespace Models;

use Classes\Model;
use Exceptions\ApplicationException;
use Interfaces\ProductInterface;
use Interfaces\ValueInterface;
use Traits\Currency;
use Traits\Value;

class Product extends Model implements ProductInterface, ValueInterface
{
    use Currency;

    use Value;

    protected $table = 'products';

    public $timestamps = false;


    /**
     * @param int $value
     */
    public function setRating(int $value) : void
    {
        if ($value < 1) {
            $value = 1;
        }

        if ($value > 5) {
            $value = 5;
        }

        $currentRating = $this->getRating() ?? 5;
        $this->rating = round(($currentRating + $value) / 2, 2);
        $this->save();
    }

    /**
     * @param $value
     */
    public function setRatedByAttribute($value)
    {
        $this->attributes['rated_by'] = json_encode($value);
    }

    /**
     * @param $value
     * @return array|mixed
     */
    public function getRatedByAttribute($value)
    {
        $data =  json_decode($value, true);

        if (!is_array($data)) {
            return [];
        }

        return $data;
    }

    /**
     * @param Customer $customer
     * @param int $rating
     * @throws ApplicationException
     */
    public function setRatedBy(Customer $customer, int $rating = 1)
    {
        if (!$this->canCustomerRate($customer))
        {
            throw new ApplicationException('This user already voted for this product!');
        }

        $data = $this->rated_by;
        $data[$customer->id] = $rating;
        $this->rated_by = $data;
        $this->save();
    }

    /**
     * @param Customer $customer
     * @return bool
     */
    public function canCustomerRate(?Customer $customer) : bool
    {
        if ($customer === null)
        {
            return false;
        }

        return !array_key_exists($customer->id, $this->rated_by);
    }

    /**
     * @param Customer $customer
     * @return int
     */
    public function getCustomerRating(Customer $customer) : int
    {
        return $this->rated_by[$customer->id] ?? 1;
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return float|null
     */
    public function getRating() : ?float
    {
        return $this->rating;
    }

    /**
     * @return string
     */
    public function getRatingAsString() : string
    {
        if (empty($this->rating)) {
            return 'no rating yet!';
        }
        return round($this->rating, 2) . '!';
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription() : string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getImage() : string
    {
        return $this->image;
    }
}
