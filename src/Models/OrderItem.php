<?php namespace Models;

use Classes\Model;
use Interfaces\ValueInterface;
use Traits\Currency;
use Traits\Value;

class OrderItem extends Model implements ValueInterface
{
    use Value;

    use Currency;

    protected $table = 'order_items';

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * @return Product
     */
    public function getProduct() : Product
    {
        return $this->product()->first();
    }
}
