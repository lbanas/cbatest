<?php require_once 'partials/header.php'; ?>

<?php require_once 'partials/navbar.php'; ?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2>Please choose delivery method</h2>

            <form action="" method="post" id="delivery_select_form">
                <label for="delivery_select">Delivery method:</label>
                <select id="delivery_select" class="custom-select" name="selected_delivery" required="required">
                    <option value="0" selected>Please choose</option>
                    <?php foreach ($params['delivery_methods'] as $delivery_method) : ?>
                        <option value="<?= $delivery_method->getId() ?>"><?= $delivery_method->getName() ?>, <?= $delivery_method->getPriceAsString() ?></option>
                    <?php endforeach; ?>
                </select>
                <button type="submit" class="btn btn-primary mt-2">Order</button>
            </form>

        </div>
    </div>
</div>
<?php require_once 'partials/footer.php'; ?>
