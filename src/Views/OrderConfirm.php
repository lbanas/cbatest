<?php require_once 'partials/header.php'; ?>

<?php require_once 'partials/navbar.php'; ?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php if (!empty($params['cart']->getProducts())) : ?>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Product name</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Price / unit</th>
                            <th scope="col">Total</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($params['cart']->getProducts() as $product) : ?>
                            <tr>
                                <td><?= $product['product']->getName(); ?></td>
                                <td><?= $product['quantity'] ?></td>
                                <td><?= $product['product']->getPriceAsString(); ?></td>
                                <td><?= number_format($product['product']->getPriceAsString() * $product['quantity'], 2) . $params['cart']->getCurrencySymbol(); ?></td>
                                <td>
                                    <?php include('partials/cart_substract.php') ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        <?php if ($params['cart']->getDeliveryMethod()) : ?>
                            <tr>
                                <td>Delivery: <?= $params['cart']->getDeliveryMethod()->getName(); ?></td>
                                <td>1</td>
                                <td><?= $params['cart']->getDeliveryMethod()->getPriceAsString(); ?></td>
                                <td><?= number_format($params['cart']->getDeliveryMethod()->getPriceAsString(), 2) . $params['cart']->getCurrencySymbol(); ?></td>
                                <td><a href="<?= $this->link('order/delivery') ?>">change</a></td>
                            </tr>
                        <?php endif ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4" class="text-right">Total:</td>
                            <td><?=  $params['cart']->getTotalAsString() ?></td>
                        </tr>
                    </tfoot>
                </table>
                <form action="<?= $this->link('order/order') ?>" method="POST">
                    <button class="btn btn-primary float-right">Order</button>
                </form>
            <?php else : ?>
                <p>Your cart is empty</p>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php require_once 'partials/footer.php'; ?>
