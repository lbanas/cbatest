<?php require_once 'partials/header.php'; ?>

<?php require_once 'partials/navbar.php'; ?>

<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="jumbotron jumbotron-fluid pt-0">
                <div class="container">
                    <h1 class="display-4"><?= $params['product']->getName(); ?></h1>
                    <div>
                        <img src="/<?= $params['product']->getImage(); ?>" alt="" class="mw-100">
                    </div>
                    <p class="lead">
                        Price: <?= $params['product']->getPriceAsString(); ?>
                        <?php require_once 'partials/product_buy.php'; ?>
                    </p>
                    <p>
                        Product rating: <?= $params['product']->getRatingAsString(); ?>
                        <?php require_once 'partials/product_rate.php'; ?>
                    </p>
                    <p>
                        <?= $params['product']->getDescription(); ?>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <?php require_once 'partials/cart.php'; ?>
        </div>
    </div>
</div>

<?php require_once 'partials/footer.php'; ?>
