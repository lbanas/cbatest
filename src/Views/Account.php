<?php require_once 'partials/header.php'; ?>

<?php require_once 'partials/navbar.php'; ?>

<div class="container">
    <div class="row">
        <div class="col-md-12">

            <?php if (!empty($params['orders'])) : ?>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">id</th>
                            <th scope="col">status</th>
                            <th scope="col">delivery method</th>
                            <th scope="col">total</th>
                            <th scope="col">created at</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php /** @var \Models\Order $order */ ?>
                        <?php foreach($params['orders'] as $order) : ?>
                            <tr>
                                <td><?= $order->id ?></td>
                                <td><?= $order->getTranslatedStatus() ?></td>
                                <td><?= $order->getDeliveryMethodAsString() ?></td>
                                <td><?= $order->getTotalAsString() ?></td>
                                <td><?= $order->created_at ?></td>
                            </tr>
                            <tr>
                                <td colspan="2">Items:</td>
                                <td colspan="3">
                                    <ul>
                                        <?php foreach($order->getItems() as $item) : ?>
                                            <li><?= $item->getProduct()->getName() ?>, <?= $item->quantity ?> item, <?= $item->getPriceAsString() ?> in total</li>
                                        <?php endforeach; ?>
                                    </ul>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php require_once 'partials/footer.php'; ?>
