<html>
<head>
    <title><?= $params['title']; ?></title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
</head>
<body>

<?php if(!empty($params['message'])) : ?>
    <div class="alert alert-info" role="alert">
        <?= $params['message'] ?>
    </div>
<?php endif; ?>
