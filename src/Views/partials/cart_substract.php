
<?php $randId = rand(0, 1000); ?>
<form action="<?= $this->link('cart/subtract') ?>" method="POST" class="form-inline">
    <input type="hidden" name="product_id" value="<?= $product['product']->getId() ?>">
    <label for="product_quantity_<?= $randId ?>" class="sr-only">Quantity</label>
    <input type="number" min="1" max="<?= $product['quantity'] ?>" id="product_quantity_<?= $randId ?>" name="product_quantity" value="1" class="form-control" />
    <button type="submit" class="btn btn-default">Subtract</button>
</form>