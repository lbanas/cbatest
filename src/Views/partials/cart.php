<div class="cart">
    <?php if ($params['customer']) : ?>
        <?php if (!empty($params['cart']->getProducts())) : ?>
            <div class="cart__products card">
                <div class="card-header">
                    Cart
                </div>
                <ul class="list-group">
                    <?php foreach($params['cart']->getProducts() as $product) : ?>
                        <li class="list-group-item">
                            <span><?= $product['product']->getName(); ?>, <?= number_format($product['product']->getPriceAsString() * $product['quantity'], 2) . $params['cart']->getCurrencySymbol(); ?></span>
                            <span class="badge badge-primary badge-pill"><?= $product['quantity'] ?> items</span>
                            <a class="float-right" href="<?= $this->link('cart/remove?id=' . $product['product']->getId()) ?>">[x]</a>
                        </li>
                    <?php endforeach; ?>
                    <?php if ($params['cart']->getDeliveryMethod()) : ?>
                        <li class="list-group-item">
                            <span>Delivery: <?= $params['cart']->getDeliveryMethod()->getName(); ?>, <?= $params['cart']->getDeliveryMethod()->getPriceAsString(); ?></span>
                        </li>
                    <?php endif ?>
                    <li class="list-group-item text-right">
                        <a href="<?= $this->link('cart/reset') ?>" id="clear_cart_anchor"class="float-left">reset cart</a>
                        <span>Total: <b><?= $params['cart']->getTotalAsString() ?></b></span>
                    </li>
                    <li class="list-group-item active">
                        <a class="btn btn-primary btn-block" href="<?= ($params['cart']->getDeliveryMethod() === null ? $this->link('order/delivery') : $this->link('order/confirm')); ?>">Order</a>
                    </li>
                </ul>
            </div>
        <?php else : ?>
            Cart is empty
        <?php endif; ?>
    <?php endif; ?>
</div>