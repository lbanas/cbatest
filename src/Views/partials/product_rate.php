<?php if ($params['customer']) : ?>
    <?php if ($params['can_rate']) : ?>
        <form action="<?= $this->link('product/rate?id=' . $params['product']->getId()) ?>" method="POST" class="form-inline">
            <label for="rate_input">Your rating: </label>
            <input id="rate_input" type="number" min="1" max="5" name="rate" class="form-control ml-2 mr-2">
            <input type="submit" value="Rate!" class="btn btn-default" />
        </form>
    <?php else : ?>
        <div>
            Your rating: <span class="text-primary"><?= $params['customer_rating'] ?></span>
        </div>
    <?php endif; ?>
<?php endif; ?>