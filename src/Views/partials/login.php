<div class="login">
    <?php if (!$params['customer']) : ?>
        <form action="<?= $this->link('customer/login') ?>" method="post" class="form-inline">
            <label for="login_login" class="sr-only">Login</label>
            <input id="login_login" placeholder="login" type="text" value="" name="login" class="form-control form-control-sm ml-2 mr-2" />
            <label for="login_password" class="sr-only">Password</label>
            <input id="login_password" placeholder="password" type="password" value="" name="password" class="form-control form-control-sm ml-2 mr-2" />
            <input type="submit" value="Log in!" class="btn btn-primary btn-sm">
        </form>
    <?php else : ?>
        Hello, <a href="<?= $this->link('account/display') ?>"><?= $params['customer']->getName(); ?></a>! Your founds: <?= $params['customer']->getFoundsAsString(); ?>. <a href="<?= $this->link('customer/logout') ?>">Logout</a>
    <?php endif; ?>
</div>