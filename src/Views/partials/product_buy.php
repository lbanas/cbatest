<?php if ($params['customer']) : ?>
    <form action="<?= $this->link('cart/add') ?>" method="POST" class="form-inline">
        <input type="hidden" name="product_id" value="<?= $params['product']->getId() ?>">
        <label for="product_quantity">Quantity:</label>
        <input id="product_quantity" type="number" name="product_quantity" value="1" class="form-control ml-2 mr-2" />
        <button type="submit" class="btn btn-primary">Add to cart</button>
    </form>
<?php endif; ?>