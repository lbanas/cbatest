<?php require_once 'partials/header.php'; ?>

<?php require_once 'partials/navbar.php'; ?>

<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="card-deck">
                <?php foreach($params['products'] as $product) : ?>
                    <div class="card float-left">
                        <a href="<?= $this->link('product/display?id=' . $product->getId()) ?>">
                            <img class="card-img-top" src="/<?= $product->getImage(); ?>" alt="" />
                        </a>
                        <div class="card-body">
                            <h5 class="card-title"><a href="<?= $this->link('product/display?id=' . $product->getId()) ?>"><?= $product->getName(); ?></a></h5>
                            <p class="card-text"><?= $product->getPriceAsString() ?></p>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="col-md-3">
            <?php require_once 'partials/cart.php'; ?>
        </div>
    </div>
</div>
<?php require_once 'partials/footer.php'; ?>
