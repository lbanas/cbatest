<?php namespace Classes;

class Response
{
    protected $url;

    protected $responseCode = 200;

    protected $message;

    /**
     * @param $code
     * @return Response
     */
    public function setResponseCode($code) : self
    {
        $this->responseCode = $code;

        return $this;
    }

    /**
     * @param string $message
     * @return Response
     */
    public function setMessage(string $message) : self
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @param $url
     * @return Response
     */
    public function setRedirectUrl($url) : self
    {
        $this->url = $url;

        return $this;
    }

    public function sendRedirectResponse() : void
    {
        header('Location: ' . $this->url ?? '/');

        if (!empty($this->message))
        {
            $_SESSION['message'] = $this->message;
        }

        exit();
    }

    public function sendResponse() : void
    {
        http_response_code($this->responseCode);

        if (!empty($this->message))
        {
            echo $this->message . '<br/><a href="/">return to homepage</a>';
        }
    }
}