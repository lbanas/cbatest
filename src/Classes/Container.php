<?php namespace Classes;

use Dotenv\Dotenv;
use Illuminate\Database\Capsule\Manager as Capsule;
use \Illuminate\Events\Dispatcher;
use \Illuminate\Container\Container as IlluminateContainer;

class Container
{
    private $dbConnection;

    private $request;

    public function __construct()
    {
        $this->initiateDotEnv();
        $this->initiateNewDbConnection();
    }

    /**
     * @param Request $request
     * @throws \Exceptions\FrontControllerException
     * @throws \Exceptions\MethodDontExistException
     */
    public function handle(Request $request) : void
    {
        $controllerClass = '\\Controllers\\' . ucfirst($request->getController()) . 'Controller';

        if (!class_exists($controllerClass))
        {
            throw new \Exceptions\FrontControllerException('Unknown controller');
        }

        $this->request = $request;

        /** @var \Classes\Controller $frontController */
        $frontController = new $controllerClass($this, $request->getAction());
        $frontController->resolve();
    }

    private function initiateNewDbConnection() : void
    {
        $this->dbConnection = new Capsule;
        $this->dbConnection->addConnection([
            'driver'    => 'mysql',
            'host'      => getenv('DB_HOST'),
            'database'  => getenv('DB_DATABASE'),
            'username'  => getenv('DB_USERNAME'),
            'password'  => getenv('DB_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ]);
        $this->dbConnection->setEventDispatcher(new Dispatcher(new IlluminateContainer));
        $this->dbConnection->setAsGlobal();
        $this->dbConnection->bootEloquent();
    }

    public function getRequest() : Request
    {
        return $this->request;
    }

    public function initiateDotEnv() : void
    {
        $dotenv = new Dotenv(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..');
        $dotenv->load();
    }
}