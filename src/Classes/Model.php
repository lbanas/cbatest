<?php namespace Classes;

use Interfaces\DatabaseModelInterface;
use \Illuminate\Database\Eloquent\Model as EloquentModel;

abstract class Model extends EloquentModel implements DatabaseModelInterface
{
    /**
     * @param $id
     * @return static|null
     */
    public static function find($id): ?DatabaseModelInterface
    {
        return static::query()->find($id);
    }

    /**
     * @return \Countable|null
     */
    public static function findAll(): ?\Countable
    {
        return static::all();
    }
}