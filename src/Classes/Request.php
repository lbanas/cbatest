<?php namespace Classes;

class Request
{
    private $post;

    private $get;

    private $cookie;

    private $controller;

    private $action;

    private $method;

    public function __construct()
    {
        $this->controller = $_GET['controller'] ?: 'Home';
        $this->action = $_GET['action'] ?: 'display';
        $this->get = array_diff_key($_GET, array_flip(['controller', 'action']));
        $this->post = $_POST;
        $this->cookie = $_COOKIE;
        $this->method = $_SERVER['REQUEST_METHOD'];
    }

    /**
     * @return null
     */
    public function getController() : ?string
    {
        return $this->controller;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @param $value
     * @param null $default
     * @return mixed
     */
    public function getPost($value, $default = null)
    {
        return ($this->post[$value] ?? $default);
    }

    /**
     * @param $value
     * @param null $default
     * @return mixed
     */
    public function getGet($value, $default = null)
    {
        return ($this->get[$value] ?? $default);
    }

    /**
     * @return array
     */
    public function getCookie(): array
    {
        return $this->cookie;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @return bool
     */
    public function isPost(): bool
    {
        return strtoupper($this->getMethod()) === 'POST';
    }
}