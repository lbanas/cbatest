<?php namespace Classes;

use Exceptions\MethodDontExistException;
use Exceptions\ViewNotFoundException;
use Interfaces\ControllerInterface;
use Models\Cart;
use Models\Customer;

abstract class Controller implements ControllerInterface
{
    /**
     * @var Container $container
     */
    private $container;

    protected $method;

    protected $parameters;

    /**
     * Controller constructor.
     * @param Container $container
     * @param $method
     * @param null $parameters
     */
    public function __construct(Container $container, $method, $parameters = null)
    {
        $this->container = $container;
        $this->method = $method;
        $this->parameters = $parameters;
    }

    /**
     * @param $template
     * @param array|null $params
     * @throws ViewNotFoundException
     */
    public function render($template, array $params = null) : void
    {
        $path = __DIR__ . DIRECTORY_SEPARATOR .'..' . DIRECTORY_SEPARATOR . 'Views' . DIRECTORY_SEPARATOR . $template . '.php';

        if (!file_exists($path))
        {
            throw new ViewNotFoundException('View not found');
        }

        if (empty($params['title']))
        {
            $params['title'] = $template;
        }

        if (isset($_SESSION['message'])) {
            $params['message'] = $_SESSION['message'];
            unset($_SESSION['message']);
        }

        ob_start();

        include_once $path;

        echo ob_get_clean();
    }

    /**
     * @return mixed
     * @throws MethodDontExistException
     */
    final public function resolve()
    {
        if (!\is_callable([$this, $this->method . 'Action']))
        {
            throw new MethodDontExistException('Unknown method');
        }

        return $this->{$this->method . 'Action'}($this->parameters);
    }

    /**
     * @param Customer $customer
     * @return Cart
     */
    public function createCartFromSession(Customer $customer) : Cart
    {
        if (isset($_SESSION['cart']))
        {
            $cart = new Cart();
            $cart->loadFromSession($_SESSION['cart']);
        }
        else
        {
            $cart = new Cart();
        }

        $cart->setCustomer($customer);

        return $cart;
    }

    /**
     * @return Container
     */
    final public function getContainer() : Container
    {
        return $this->container;
    }

    /**
     * @return Request
     */
    final public function getRequest() : Request
    {
        return $this->getContainer()->getRequest();
    }

    /**
     * @param $url
     */
    public function redirect($url) : void
    {
        $response = new Response();
        $response
            ->setRedirectUrl($url)
            ->sendRedirectResponse()
        ;
    }

    /**
     * @param $url
     * @param string $message
     */
    public function redirectWithMessage($url, string $message) : void
    {
        $response = new Response();
        $response
            ->setRedirectUrl($url)
            ->setMessage($message)
            ->sendRedirectResponse()
        ;
    }

    /**
     * @param $url
     * @return string
     */
    public function link($url) : string
    {
        return getenv('PROJECT_DIRECTORY') . $url;
    }
}