function PageController() {
    this.deliverySelectFormSelector = '#delivery_select_form';
    this.clearCartSelector = '#clear_cart_anchor';

    this.init = function () {
        this.watchDeliveryForm();
        this.watchClearCartAnchor();
    };

    this.watchDeliveryForm = function () {
        "use strict";

        $(this.deliverySelectFormSelector).on('submit', function (event) {
            var $select = $(this).find('select');

            if ($select.val() <= 0) {
                event.preventDefault();
                alert('please select delivery method');
            }
        });
    };

    this.watchClearCartAnchor = function () {
        "use strict";

        $(this.clearCartSelector).on('click', function () {
            return confirm('Are you sure that you want to reset cart?');
        });
    }
}

$(document).ready(function() {
    "use strict";

    var controller = new PageController();
    controller.init();
});