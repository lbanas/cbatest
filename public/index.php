<?php

require_once '../vendor/autoload.php';

session_start();

try
{
    $request = new \Classes\Request();
    $container = new \Classes\Container();
    $container->handle($request);
}
catch (\Exceptions\ApplicationException $exception)
{
    $response = new \Classes\Response();
    $response->setMessage($exception->getMessage())
        ->setResponseCode(403)
        ->sendResponse()
;
}
catch (\Exceptions\FrontControllerException $frontControllerException)
{
    echo $frontControllerException->getMessage();
}

exit();